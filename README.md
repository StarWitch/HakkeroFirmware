# Hakkero Firmware

For a 3D printed bright and shiny Hakkero.

Work in progress as of January 2021. Will be posting 3D models, PCB CAD files, etc. in the future. (Or, bug me on Twitter if I haven’t yet.)

Currently uses:

* Adafruit CircuitPlayground Bluefruit
* Adafruit NeoPixel 3W LED
* Custom APA102 (“DotStar”) LEDs on a custom PCB
* Adafruit PowerBoost 1000
* Adafruit LC709203F battery fuel gauge
* Custom-designed 3D printed enclosure
* Marisa Kirisame’s limitless enthusiasm
