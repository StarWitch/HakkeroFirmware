#ifndef LIGHTS_H
#define LIGHTS_H

#include <FastLED.h>

extern void SetupLights();
extern void UpdateLights(CRGBPalette16 *BLEPalette);

#define HAKKERO_BRIGHTNESS  64
#define UPDATES_PER_SECOND 1000

// APA102 pixels on custom PCBs
#define CLOCK_PIN A1
#define DATA_PIN A3
#define NUM_HAKKERO_LEDS 124
extern CRGB hakkero_leds[NUM_HAKKERO_LEDS];

// 3-Watt RGB LED 
#define NEOPIXEL_PIN A2
#define NUM_HIGHWATT_LEDS 1
extern CRGB pixel_leds[NUM_HIGHWATT_LEDS];

// built-in LEDs on the CircuitPlayground Express
#define CIRCUITPLAYGROUND_PIN 8
#define NUM_CPX_LEDS 10
extern CRGB cpx_leds[NUM_CPX_LEDS];

// FastLED helpers
extern CRGBPalette16 currentPalette;
extern TBlendType    currentBlending;

#endif