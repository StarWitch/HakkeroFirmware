// fuel gauge chip
#include "sensors.h"

Adafruit_LC709203F lc;

void SetupSensors()
{
    Wire.begin();
    if (lc.begin())
    {
        lc.setThermistorB(3950);
        lc.setPackSize(lc709203_adjustment_t(0x45));
        lc.setAlarmVoltage(3.8);
        
        Serial.println("Detected battery");
        Serial.print("Batt Voltage: ");
        Serial.println(lc.cellVoltage(), 3);
        Serial.print("Batt Percent: ");
        Serial.println(lc.cellPercent(), 1);
    }
    else
    {
        Serial.println(F("Couldnt find Adafruit LC709203F?\nMake sure a battery is plugged in!"));
    }
}