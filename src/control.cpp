#include "control.h"
#ifdef BLUETOOTH
#include <bluefruit.h>

// BLE Service
BLEDfu  bledfu;
BLEDis  bledis;
BLEUart bleuart;

static void connectCallbackBLE(uint16_t conn_handle)
{
  // Get the reference to current connection
  BLEConnection* connection = Bluefruit.Connection(conn_handle);

  char central_name[32] = { 0 };
  connection->getPeerName(central_name, sizeof(central_name));
}

static void setModeBLE() {

}

void SetupBLE() {
  // Init Bluefruit
  Bluefruit.begin();
  Bluefruit.setTxPower(4);    // Check bluefruit.h for supported values
  Bluefruit.setName("Mini-Hakkero");
  Bluefruit.Periph.setConnectCallback(connectCallbackBLE);

  // To be consistent OTA DFU should be added first if it exists
  bledfu.begin();

  // Configure and Start Device Information Service
  bledis.setManufacturer("Kirisame Magic Shop");
  bledis.setModel("Mini-Hakkero v0.5");
  bledis.begin();  

  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  
  // Include bleuart 128-bit uuid
  Bluefruit.Advertising.addService(bleuart);

  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();
  
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
}
#endif

CRGBPalette16* GetModeBLE() {
  CRGBPalette16 pallete = RainbowColors_p;
  return &pallete;
}

