#ifndef HAKKERO_H
#define HAKKERO_H

#include <Arduino.h>

#include "sensors.h"
#include "lights.h"
#include "control.h"

// debug helpers
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)

#endif