#include "hakkero.h"

void setup() {
  SetupLights();
  //SetupSensors();
  #ifdef BLUETOOTH
  SetupBLE();
  #endif
}

void loop() {
  UpdateLights(GetModeBLE());
}