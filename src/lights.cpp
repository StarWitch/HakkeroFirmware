#include "lights.h"

CRGB hakkero_leds[NUM_HAKKERO_LEDS];
CRGB pixel_leds[NUM_HIGHWATT_LEDS];
CRGB cpx_leds[NUM_CPX_LEDS];

// FastLED helpers
CRGBPalette16 currentPalette;
TBlendType    currentBlending;

static void fillLEDsFromPaletteColors(uint8_t colorIndex)
{
    uint8_t brightness = 255;

    for( int i = 0; i < NUM_HAKKERO_LEDS; i++) {
        hakkero_leds[i] = ColorFromPalette(currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 3;
    }
    for( int x = 0; x < NUM_CPX_LEDS; x++) {
        cpx_leds[x] = ColorFromPalette(currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 9;
    }
    
    pixel_leds[0] = ColorFromPalette(currentPalette, colorIndex, brightness, currentBlending);
    colorIndex += 3;

}

static void changePalettePeriodically()
{
    uint8_t secondHand = (millis() / 1000) % 60;
    static uint8_t lastSecond = 99;

    if( lastSecond != secondHand) {
        lastSecond = secondHand;
        if( secondHand ==  0)  { currentPalette = RainbowColors_p;         currentBlending = LINEARBLEND; }
        if( secondHand == 15)  { currentPalette = RainbowStripeColors_p;   currentBlending = NOBLEND;  }
        if( secondHand == 30)  { currentPalette = RainbowStripeColors_p;   currentBlending = LINEARBLEND; }
        if( secondHand == 45)  { currentPalette = PartyColors_p;           currentBlending = LINEARBLEND; }
    }
}

void SetupLights() {
  delay(1000); // safety
  FastLED.addLeds<DOTSTAR, DATA_PIN, CLOCK_PIN, RGB>(hakkero_leds, NUM_HAKKERO_LEDS);
  FastLED.addLeds<WS2812, NEOPIXEL_PIN, RGB>(pixel_leds, NUM_HIGHWATT_LEDS);
  FastLED.addLeds<WS2812, CIRCUITPLAYGROUND_PIN, RGB>(cpx_leds, NUM_CPX_LEDS);
  FastLED.setBrightness(HAKKERO_BRIGHTNESS);
}

void UpdateLights(CRGBPalette16 *BLEPalette) {
    //currentPalette = *BLEPalette;
    //currentBlending = LINEARBLEND;
    changePalettePeriodically();

    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */

    fillLEDsFromPaletteColors(startIndex);
    FastLED.show();
    FastLED.delay(200 / UPDATES_PER_SECOND);
}